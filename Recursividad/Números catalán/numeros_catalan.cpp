#include <iostream>
#include <cstdlib>

using namespace std;

int Catalan(int n)
{
    if (n <= 0)
        return 1;   
    else
        return (2 * (2*n - 1) * Catalan(n - 1)) / (n + 1);               
}

int main()
{
    cout << "Serie de números de catalán" << endl;
    int n ;       
     
    cout << "Ingrese número: ";
    cin >> n;
    cout << "El " << n << " número catalán es: " << Catalan(n) << endl;
    cout << "Los primeros " << n << " números catalanes son: " << endl;      
    for(int i=0; i<n; i++)
    {
        cout << Catalan(i);
        cout <<", ";
    }
    cout << endl;
    return 0;
}
