/*
* C++ - Algoritmo de Boyer Moore Horspool
*
* Copyright 2014 Martin Cruz Otiniano
*
* Site: www.marcsdev.com
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define MAXT 1001
#define MAXP 1000

using namespace std;

char        texto[MAXT] ;
char        patron[MAXP] ;
signed int  rep  ;            // numero de veces encontrado 
signed int  comp ;            // numero de comparaciones 

/******************** Busqueda por Boyer Moore Horspool ****************************/

void preBMBc(char *P, int m, int tabla[])
{
    int i;
    for (i = 0; i < 256; ++i)
        tabla[i] = m;                  

    for (i = 0; i < m - 1; ++i)
    {
        tabla[P[i]] = m - i - 1;  

    }  

} 

void BMH( char *T, int n , char *P, int m)
{
    int  j , bmBC[256] ;
    char c ;

    preBMBc(P, m, bmBC) ;  // Preprocesamiento 

    // Búsqueda 

    j = 0;
    while (j <= n - m)
    {
        c = T[j + m - 1] ;

        if ( P[m - 1] == c && memcmp(P, T + j, m - 1) == 0 )
        {
             cout<<" * Encontrado en : "<< j + 1 << endl; 
             rep ++ ;
        }

        j = j + bmBC[c] ;     comp ++ ;                                   

    }
}

/*************************** Funcion Principal *****************************/

int main()
{

    float t1 , t2, tiempo ;  // variables pata tiempo de busqueda
    rep = 0 ;

    cout<<endl ;
   cout<<"\t º ---------------------------------------------- º "<< endl ;
    cout<<"\t º ALGORITMO BOOYER M. HORSPOOL º "<< endl ;
    cout<<"\t º ---------------------------------------------- º "<< endl ;


    cout<<endl<<" Ingrese Texto : \n\t\t" ;
    gets( texto ) ;

    cout<<endl<<" Ingrese Patron : \n\t\t" ;
    gets( patron ) ;

    int n  = strlen( texto ) ;
    int m  = strlen( patron ) ;

    cout<<"\n__________________________________________________________"<<endl<<endl;   

    t1 = clock();
    BMH( texto , n , patron , m ) ;
    t2 = clock();

    cout<<"\n__________________________________________________________"<<endl<<endl;

    tiempo = (t2-t1)/100000 ;
    cout<<endl<<" >> Tiempo de busqueda : "<< tiempo ;

    if(rep  == 0)
        cout<<endl<<endl<<" >> Patron no encontrado ..! " ;
    else
        cout<<endl<<endl<<" >> Ocurrencias : "<< rep ;    

    cout<<endl<<endl<<" >> Comparaciones : "<< comp ; 

    cout<<"\n\n__________________________________________________________"<<endl<<endl;

    
    return 0;
}