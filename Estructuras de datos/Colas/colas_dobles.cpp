/*
* C++ - Colas Dobles
* Copyright 2014 Martin Cruz Otiniano
* Description: Inserta y elimina por delante y detras de la cola
* Site: www.marcsdev.com
*/

#include <iostream>
#include <stdlib.h>
using namespace std;


/*                Estructura de los nodos de la cola       
----------------------------------------------------------------------*/
struct nodo
{
    char dato;
    struct nodo *sgte;
};

/*                      Estructura de la cola      
----------------------------------------------------------------------*/
struct cola
{
    nodo *delante;
    nodo *atras  ;
};

/*                         Crear Nodo     
----------------------------------------------------------------------*/
struct nodo *crearNodo( char x)
{
    struct nodo *nuevoNodo = new(struct nodo);
    nuevoNodo->dato = x;
    return nuevoNodo;
};

/*                        Encolar elemento       
----------------------------------------------------------------------*/
void encolar( struct cola &q, char x, int pos )
{
     struct nodo *aux = crearNodo(x);
     
     if( pos==1 )
     {
          if( q.delante==NULL)
          {
               aux->sgte = q.delante;
               q.delante = aux;
               q.atras   = aux; 
          }
          else
          {
               aux->sgte = q.delante;
               q.delante = aux;
          }
     }
     else 
     {
          if( q.atras==NULL )
          {
               aux->sgte = q.atras;
               q.delante = aux;
               q.atras   = aux;
          }
          else
          {
               aux->sgte = (q.atras)->sgte;
               (q.atras)->sgte = aux;
          }
     }
}

/*                        Desencolar elemento       
----------------------------------------------------------------------*/
char desencolar( struct cola &q, int pos )
{
     char __c ;
     struct nodo *aux = q.delante;
     
     if( pos==1 )
     {
          __c = (q.delante)->dato;
          q.delante = aux->sgte;
          delete(aux);
     }
     else 
     {
          __c = (q.atras)->dato;
          
          while( aux->sgte!=q.atras )
               aux = aux->sgte;
               
          aux->sgte = (q.atras)->sgte;
          delete(q.atras);
          q.atras = aux;
     }
     
     return __c;
}

/*                        Mostrar Cola       
----------------------------------------------------------------------*/
void muestraCola( struct cola q )
{
     struct nodo *aux;
     
     aux = q.delante;
          
     while( aux != NULL )
     {
            cout<<"   "<< aux->dato ;
            aux = aux->sgte;
     }    
}

/*                        Menu de opciones     
----------------------------------------------------------------------*/
void menu()
{
    cout<<"\n\t IMPLEMENTACION DE COLAS DOBLES EN C++\n\n";
    cout<<" 1. INSERTAR                              "<<endl;
    cout<<" 2. ELIMINAR                              "<<endl;
    cout<<" 3. MOSTRAR COLA                          "<<endl;
    cout<<" 4. SALIR                                 "<<endl;

    cout<<"\n INGRESE OPCION: ";
}

/*                        Funcion Principal        
----------------------------------------------------------------------*/
int main()
{
    struct cola q;
    
    q.delante = NULL;
    q.atras   = NULL;
    
    char c;     // caracter a encolar
    char x ;    // caracter que devuelve la funcion pop (desencolar)
    int op;     // opcion del menu
    int pos;    // posicion de isertar o eliminar (inicio o fin)
    
    do
    {
        menu();  cin>> op;

        switch(op)
        {
            case 1:
                 cout<< "\n Ingrese caracter: "; 
                 cin>> c;
                 
                 cout<<"[1] Inserta al inicio " <<endl;
                 cout<<"[2] Inserta al final  " <<endl;
                 cout<<"Opcion : ";
                 cin>> pos;
                 
                 encolar( q, c, pos );
                 
                 cout<<"\n\n\t\tNumero '" << c << "' encolado...\n\n";
                 
            break; 


            case 2:
                 cout<<"[1] Elimina al inicio " <<endl;
                 cout<<"[2] Elimina al final  " <<endl;
                 cout<<"Opcion : ";
                 cin>> pos;
                 
                 x = desencolar( q, pos );
                 
                 cout<<"\n\n\t\tNumero '"<< x <<"' desencolado...\n\n";
                 
            break;
                  

            case 3:
                 cout << "\n\n MOSTRANDO COLA\n\n";
                 
                 if(q.delante!=NULL) 
                     muestraCola( q );
                 else   
                     cout<<"\n\n\tCola vacia...!" << endl;
                 
            break;
         }

        cout<<endl<<endl;
        cin.ignore();
        cin.get();
        system("clear"); //Limpiar pantalla en linux

    } while(op!=4);
    
    return 0;
}